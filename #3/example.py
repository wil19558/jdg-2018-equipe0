import click
import timeit

@click.command()
def sort():

    start_time = timeit.default_timer()
    numbers = None
    with open('to_be_sorted.txt', 'r') as f:
        lines = f.readlines()
        numbers = [None]*len(lines)
        for i in range(len(lines)):
            numbers[i] = int(lines[i])

    numbers = sorted(numbers)

    with open('sorted.txt', 'w+') as f:
        for n in numbers:
            f.write(str(n) + '\n')
    elapsed = timeit.default_timer() - start_time
    print(elapsed)


if __name__ == '__main__':
    sort()
