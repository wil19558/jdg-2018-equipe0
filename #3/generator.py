import click
import random

@click.command()
@click.option('--size', default=100000, help='Number of lines to the unsorted file')
def generate_file(size):
    numbers = range(size)
    random.shuffle(numbers)
    
    with open('result.txt', 'w+') as f:
        for n in numbers:
            f.write(str(n) + '\n')


if __name__ == '__main__':
    generate_file()
