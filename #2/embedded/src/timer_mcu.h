/*
 * timer_mcu.h
 *
 *  Created on: Sep 15, 2016
 *      Author: William Laroche
 */

#ifndef TIMER_TIMER_MCU_H_
#define TIMER_TIMER_MCU_H_

#include <timer.h>

extern volatile TimerType tenthms_count;

void Timer_Mcu_Init();
void TIM_10kHzCallback();

#endif /* TIMER_TIMER_MCU_H_ */
