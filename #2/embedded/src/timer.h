/*
 * timer.h
 *
 *  Created on: Jul 12, 2016
 *      Author: WILL
 */

#ifndef TIMER_DRV_H_
#define TIMER_DRV_H_


#define TIMER_FREQUENCY 10000
#define TIMER_S(s) (TIMER_FREQUENCY*s)
#define TIMER_MS(ms) ((TIMER_FREQUENCY/1000)*ms)
#define TIMER_TENTH_MS(tms) ((TIMER_FREQUENCY/10000)*tms)

#include <stdint.h>

typedef uint32_t TimerType;
typedef int8_t boolean;
#define FALSE 0
#define TRUE 1


void Timer_Init();

void Timer_Start(TimerType* handle);

boolean Timer_Increment(TimerType* handle, TimerType duration);

boolean Timer_Is_Expired(TimerType* handle, TimerType duration);

boolean Timer_If_Expired_Increment(TimerType* handle, TimerType duration);

TimerType Timer_Diff(TimerType most_recent, TimerType oldest);

TimerType Timer_CurrentCountTenthMS();


#endif /* TIMER_DRV_H_ */
