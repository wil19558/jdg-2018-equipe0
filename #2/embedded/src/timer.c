/*
 * timer.c
 *
 *  Created on: Jul 12, 2016
 *      Author: WILL
 */

#include <timer.h>
#include "timer_mcu.h"

void Timer_Init(){
	Timer_Mcu_Init();
}

void Timer_Start(TimerType* handle){
	*handle = tenthms_count;
}

boolean Timer_Increment(TimerType* handle, TimerType duration){
	*handle = *handle + duration;
	return Timer_Is_Expired(handle, duration);
}

boolean Timer_Is_Expired(TimerType* handle, TimerType duration){
	return (TimerType)(tenthms_count - *handle) >= duration;
}

boolean Timer_If_Expired_Increment(TimerType* handle, TimerType duration){
	boolean expired = FALSE;
	if(Timer_Is_Expired(handle, duration)){
		Timer_Increment(handle, duration);
		expired = TRUE;
	}
	return expired;
}

TimerType Timer_Diff(TimerType most_recent, TimerType oldest){
	return most_recent - oldest;
}

TimerType Timer_CurrentCountTenthMS(){
	return tenthms_count;
}
